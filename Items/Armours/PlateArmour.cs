﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Armours
{
    public class PlateArmour : Armour
    {
        static CommonAttributes baseBonus = new CommonAttributes
        {
            Health = 30,
            Strength = 3,
            Dexterity = 1,
            Intelligence = 0,
        };
        static CommonAttributes scaling = new CommonAttributes
        {
            Health = 12,
            Strength = 2,
            Dexterity =1,
            Intelligence = 0,
        };

        public PlateArmour(string name, int level, ArmorSlot slot) : base(name, level,slot,baseBonus,scaling)
        {}
    }
}
