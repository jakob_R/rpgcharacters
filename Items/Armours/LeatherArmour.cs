﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Armours
{
    public class LeatherArmour : Armour
    {
        static CommonAttributes baseBonus = new CommonAttributes
        {
            Health = 10,
            Strength = 0,
            Dexterity = 1,
            Intelligence = 3,
        };
        static CommonAttributes scaling = new CommonAttributes
        {
            Health = 5,
            Strength = 0,
            Dexterity = 1,
            Intelligence = 2,
        };

        public LeatherArmour(string name, int level, ArmorSlot slot) : base(name, level,slot, baseBonus, scaling)
        {}
    }
}
