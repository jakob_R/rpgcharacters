﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Armours
{
    public abstract class Armour : Item
    {
        //Variables for all armours 
        public CommonAttributes EffectiveBonus { get; set; }
        public CommonAttributes ScaledBonus { get; set; }
        public ArmorSlot Slot { get; set; } 
        protected Armour(string name, int level,ArmorSlot slot, CommonAttributes baseBonus, CommonAttributes scaling)
        {            
            //sets variables
            Name = name;
            Level = level;
            Slot = slot;

            //Slot Multiplier
            double slotMultiplier = (double)(slot)/ (100);
            //Calculates Effective bonus from baseBonus, scaling, level and slotmultiplier
            EffectiveBonus = new CommonAttributes
            {
                Health = (int)((baseBonus.Health + scaling.Health * level) * slotMultiplier),
                Strength = (int)((baseBonus.Strength + scaling.Strength * level)*slotMultiplier),
                Dexterity = (int)(( baseBonus.Dexterity + scaling.Dexterity * level)*slotMultiplier),
                Intelligence = (int)((baseBonus.Intelligence + scaling.Intelligence * level)*slotMultiplier),
            };
        }
        public virtual void PrintArmourStats()
        {
            Console.WriteLine($"Armour: {Name}\n" +
                $"Slot: {Slot}\n" +
                $"Level: {Level}\n" +
                $"Bonus Health : {EffectiveBonus.Health}\n" +
                $"Bonus Strength : {EffectiveBonus.Strength}\n" +
                $"Bonus Dexterity : {EffectiveBonus.Dexterity}\n" +
                $"Bonus Intelligence : {EffectiveBonus.Intelligence}\n");
        }
    }

    //armour slots and their stat scaling
    public enum ArmorSlot
    {
        Legs=60,
        Body=100,
        Head=80
    }
}
