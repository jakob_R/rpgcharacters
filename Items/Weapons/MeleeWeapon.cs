﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Weapons
{
    class MeleeWeapon : Weapon
    {
        private int baseDamage = 15;
        private int levelScaling = 2;
        public MeleeWeapon(string name, int level) : base(name, level) 
        {
            WeaponDamage = baseDamage + levelScaling * level;
        }

        public override int AttackingDamage(Hero attacker)
        {
            return (int)(attacker.EffectiveStats.Strength * 1.5 + WeaponDamage);
        }
    }
}
