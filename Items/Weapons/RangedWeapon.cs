﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Weapons
{
    class RangedWeapon : Weapon
    {
        private int baseDamage = 5;
        private int levelScaling = 3;
        public RangedWeapon(string name, int level) : base(name, level) 
        {
            WeaponDamage = baseDamage + levelScaling * level;
        }

        public override int AttackingDamage(Hero attacker)
        {
            return (int)(attacker.EffectiveStats.Dexterity * 2 + WeaponDamage);
        }
    }
}
