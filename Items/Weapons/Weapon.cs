﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Weapons
{
    public abstract class Weapon : Item
    {
        //Weapondamage 
        public double WeaponDamage { get; set; }
        protected Weapon(string name, int level) 
        {
            Name = name;
            Level = level;
        }
        //Calculates damage dealt implemented in each weapon class
        public abstract int AttackingDamage(Hero attacker);

        public virtual void PrintWeaponStats()
        {
            Console.WriteLine($"Weapon: {Name}\nLevel: {Level}\nWeaponDamage: {WeaponDamage}\n");
        }

    }
}
