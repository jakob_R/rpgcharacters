﻿using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items.Weapons
{
    class MagicWeapon : Weapon
    {
        private int baseDamage = 25;
        private int levelScaling = 2;
        public MagicWeapon(string name, int level) : base(name , level)
        {
            WeaponDamage = baseDamage + levelScaling * level;
        }

        public override int AttackingDamage(Hero attacker)
        {
            return (int)(attacker.EffectiveStats.Intelligence * 3 + WeaponDamage);
        }
    }
}
