﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Items
{
    //abstract Item class
    public abstract class Item
    {
        // Attributes common for all items
        public string Name { get; set; }
        public int Level { get; set; }
    }
}
