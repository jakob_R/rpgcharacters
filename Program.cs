﻿using RPGCharacters;
using RPGCharacters.Heroes;
using RPGCharacters.Items.Armours;
using RPGCharacters.Items.Weapons;
using System;

namespace RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            //Generate characters
            HeroFactory heroFactory = new HeroFactory();
            Hero mage1 = heroFactory.GetHero(HeroFactory.HeroClass.Mage);
            Hero ranger1 = heroFactory.GetHero(HeroFactory.HeroClass.Ranger);
            Hero warrior1 = heroFactory.GetHero(HeroFactory.HeroClass.Warrior);
            Hero warrior2 = heroFactory.GetHero(HeroFactory.HeroClass.Warrior);

            //Giving characters xp making them level up

            //Gives ranger 2000xp, will be lvl 12 
            Console.WriteLine(ranger1.GetHeroInfo());
            ranger1.AddXP(2000);
            Console.WriteLine(ranger1.GetHeroInfo());
            Console.WriteLine(mage1.GetHeroInfo());


            //Gives warrior 3000xp, will be lvl 15
            warrior1.AddXP(3000);
            //Gives warrior 1200xp, will be lvl 9
            warrior2.AddXP(1200);




            //Create weapon and prints their stats
            Weapon meleeWeapon1 = new MeleeWeapon("Great Axe of the Exiled",5 );
            meleeWeapon1.PrintWeaponStats();
            Weapon rangedWeapon1 = new RangedWeapon("Long Bow of the Lone Wolf", 10);
            rangedWeapon1.PrintWeaponStats();
            Weapon magicWeapon1 = new MagicWeapon("Wand of the Sorcerer", 10);
            magicWeapon1.PrintWeaponStats();

            //Create armour and  print stats
            Armour cloth = new ClothArmour("Cloth Leggings of the Magi",10,ArmorSlot.Legs);
            Armour leather = new LeatherArmour("Leather Helmet of the lether", 10, ArmorSlot.Head);
            Armour platearmor = new PlateArmour("Plate Chest of the Juggernaut", 15, ArmorSlot.Body);        
            Armour platearmor2 = new PlateArmour("Plate Chest of the Juggernaut", 5, ArmorSlot.Body);

            platearmor2.PrintArmourStats();
            leather.PrintArmourStats();   
            cloth.PrintArmourStats();
            platearmor.PrintArmourStats();

            //equips two different armors and displays stats after each of them is equiped
            warrior1.EquipArmour(platearmor);
            Console.WriteLine(warrior1.GetHeroInfo());
            warrior1.EquipArmour(platearmor2);
            Console.WriteLine(warrior1.GetHeroInfo());

            //equips armor for the other slots and prints stats
            warrior1.EquipArmour(leather);
            warrior1.EquipArmour(cloth);            
            Console.WriteLine(warrior1.GetHeroInfo());

            //EQuips warrior2 with armor and weapon and attacks
            warrior2.EquipWeapon(meleeWeapon1);
            warrior2.EquipArmour(platearmor2);
            meleeWeapon1.PrintWeaponStats();
            warrior2.Attack();

            //Attacking other hero 
            Console.WriteLine("\n<--- Attacking hero  --->\n");
            Console.WriteLine(warrior1.GetHeroInfo());
            warrior2.Attack(warrior1);
            Console.WriteLine(warrior1.GetHeroInfo());

        }
    }
}
