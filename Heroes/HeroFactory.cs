﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    // hero factory 
    public class HeroFactory
    {
        public enum HeroClass
        {
            Mage,
            Ranger,
            Warrior
        }
        // returns a Hero instance 
        public Hero GetHero(HeroClass heroClass)
        {
            switch (heroClass)
            {
                case HeroClass.Mage:
                    return new Mage();
                case HeroClass.Ranger:
                    return new Ranger();
                case HeroClass.Warrior:
                    return new Warrior();
                default:
                    return null;
            }
        }
    }
}