﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    //Mage hero 
    class Mage : Hero
    {
        
        //Calls parent constructor with mage spesific stats 
        public Mage() : base(
            new CommonAttributes
            {
                Health = 100,
                Strength = 2,
                Dexterity = 3,
                Intelligence = 10,
            }   
            , 
            new CommonAttributes
            {
                Health = 15,
                Strength = 1,
                Dexterity = 2,
                Intelligence = 5,
            })
        {}

       

        public override string GetHeroInfo()
        {
            return "\nMage " + base.GetHeroInfo();
        }
        
    }
}