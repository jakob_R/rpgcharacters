﻿using RPGCharacters.Items.Weapons;
using RPGCharacters.Items.Armours;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    public abstract class Hero
    {

        // Variables common for all heroes
        //Stats 
        public CommonAttributes BaseStats;
        public CommonAttributes EffectiveStats;
        public CommonAttributes levelUpStats;
        public int Level { get; set; }
        public int CurrentXp { get; set; }
        public int XpToLevelUp { get; set; }
        //Items
        public Weapon Weapon {get; set;}
        public Armour HeadArmor { get; set; }
        public Armour BodyArmor { get; set; }
        public Armour LegArmor { get; set; }
        //constructors
        protected Hero(CommonAttributes basestats,CommonAttributes levelUpStats)
        {
            //sets properties
            this.BaseStats = basestats;
            this.levelUpStats = levelUpStats;
            EffectiveStats = basestats;
            Level = 1;
            XpToLevelUp = 100;
        }
        //Shared behaviour 

        //Called to Increase Level
        public void IncreaseLevel()
        {
            // Updates basestats
            BaseStats.Health += levelUpStats.Health;
            BaseStats.Strength += levelUpStats.Strength;
            BaseStats.Dexterity += levelUpStats.Dexterity;
            BaseStats.Intelligence += levelUpStats.Intelligence;
            //increase level
            Level++;
            //updates xptolevelup
            XpToLevelUp = (int)(XpToLevelUp * 1.1);
        }
        public void AddXP(int amount)
        {
            //if xptolevelup is higher than amount: update XpToLevelUp and return 
            while (amount > XpToLevelUp)
            {
                amount -= XpToLevelUp;
                IncreaseLevel();
                UpdateEffectiveStats();
            }
            if (amount < XpToLevelUp)
            {
                XpToLevelUp -= amount;
                return;
            }
        }
        //Equips hero with  Weapon  
        public void EquipWeapon(Weapon weapon)
        {
            //Equips weapon if hero level is same or higher than weapon
            if (Level >= weapon.Level)
            {
                Weapon = weapon;
            } else
            {
                Console.WriteLine("Could not equip weapon: hero level to low");
            }

        }
        //Equips hero with Armor
        public void EquipArmour(Armour armour) 
        {
            //return if hero level is lower than armour level
            if (Level < armour.Level)
            {                
                Console.WriteLine("Could not equip armour : hero level to low");
                return;
            }
            //equips armour based on which slot it belongs
            switch (armour.Slot)
            {
                case ArmorSlot.Head:
                    HeadArmor = armour;
                    break;
                case ArmorSlot.Body:
                    BodyArmor = armour;
                    break;
                case ArmorSlot.Legs:
                    LegArmor = armour;
                    break;
                default:
                    break;
            }
            //Updates stats 
            UpdateEffectiveStats();
        }

        //updates effective stats based on basestats and equipment
        private void UpdateEffectiveStats()
        {   
            //Sets equal to base 
            EffectiveStats = BaseStats;            
            // Checks for equiped armour and adds their bonus stats  
            if (HeadArmor!=null)
            {
                EffectiveStats.Health += HeadArmor.EffectiveBonus.Health;
                EffectiveStats.Strength += HeadArmor.EffectiveBonus.Strength;
                EffectiveStats.Dexterity += HeadArmor.EffectiveBonus.Dexterity;
                EffectiveStats.Dexterity += HeadArmor.EffectiveBonus.Intelligence;
            } 
            if (BodyArmor != null) 
            {
                EffectiveStats.Health += BodyArmor.EffectiveBonus.Health;
                EffectiveStats.Strength += BodyArmor.EffectiveBonus.Strength;
                EffectiveStats.Dexterity += BodyArmor.EffectiveBonus.Dexterity;
                EffectiveStats.Dexterity += BodyArmor.EffectiveBonus.Intelligence;
            }
            if (LegArmor != null)
            {
                EffectiveStats.Health += LegArmor.EffectiveBonus.Health;
                EffectiveStats.Strength += LegArmor.EffectiveBonus.Strength;
                EffectiveStats.Dexterity += LegArmor.EffectiveBonus.Dexterity;
                EffectiveStats.Dexterity += LegArmor.EffectiveBonus.Intelligence;
            }
        }
        //print how much damage an attack does
        public void Attack()
        {
            Console.WriteLine("\nAttacking for "+Weapon.AttackingDamage(this));
        }

        public void Attack(Hero heroToAttack)
        {
            int damage = Weapon.AttackingDamage(this);
            Console.WriteLine("\nAttacking for " + damage);
            heroToAttack.EffectiveStats.Health -= damage;
        }

        public virtual string GetHeroInfo()
        {
            return ($" Details \nHealth: { EffectiveStats.Health}\nStrength: { EffectiveStats.Strength}\nDexterity: {EffectiveStats.Dexterity}\nIntelligence: {EffectiveStats.Intelligence}\nLevel: { Level }\nXP to level up: {XpToLevelUp}\n");
        }        
    }
}
