﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    //Common Stats/Attributes
    public struct CommonAttributes
    {
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }        
    }
}
