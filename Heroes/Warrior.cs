﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    //Warrior hero 
    class Warrior : Hero
    {
        public Warrior() : base(
            new CommonAttributes
            {
                Health = 150,
                Strength = 10,
                Dexterity = 3,
                Intelligence = 1,
            }
            ,
            new CommonAttributes
            {
                Health = 30,
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
            })
        { }


        public override string GetHeroInfo()
        {
            return "\nWarrior " + base.GetHeroInfo();
        }
       
    }
}
