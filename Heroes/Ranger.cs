﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters.Heroes
{
    //Ranger hero 
    class Ranger : Hero
    {
        public Ranger():base(
            new CommonAttributes
            {
                Health = 120,
                Strength = 5,
                Dexterity = 10,
                Intelligence = 2,
            }   
            , 
            new CommonAttributes
            {
                Health = 20,
                Strength = 2,
                Dexterity = 5,
                Intelligence = 1,
            })
        {}

        public override string GetHeroInfo()
        {
            return "\nRanger " + base.GetHeroInfo();
        }

    }
}
